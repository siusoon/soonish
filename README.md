# soonish theme for PicoCMS

Figure 1: ![](soonish.png)

Figure 2:![](soonish2.png)

By: Winnie Soon, 2022

Licence: The MIT License (MIT)

## Description

soonish is a free and open source portfolio theme designed for PicoCMS, a flat file CMS, with the attention to file structures, queer layout design, web responsive feature.

soonish was forked from another two-column theme, simpleTwo, with additional features added, and an automatically generating directory listings for subfolders feature adapted from pico-femto.

Features:

1. Automatically generate a list of markdown files within the same folder on index.md (adapt the code from [pico-femto](https://github.com/randomchars42/pico-femto)) (see Figure 2)
2. Following [pico-femto](https://github.com/randomchars42/pico-femto), content/files in a folder called archive will not be listed on the navbar.
3. Only show the title of the index.md file within each folder on the navbar
4. The file with "Tag: hide" won't show in the generated file list (in #1)
5. Check against the folder directory for all sub files, and this will help to mark the current location in the navbar (<<).
6. The addition of Libre Fonts
7. CSS: animated colorful site title (CSS), table color, font size, color, background gradient, etc
8. Simple responsive web for mobile devices (with CSS)

NB: the order of the navbar is subjected to the alphabetical orders of the folders, but the order of the files within a folder will be listed according to the date order.

## Details:

A live version of the theme can be seen at [siusoon.net](http://siusoon.net)

### Example

#### Current directory:

```yaml
  about/index.md (About)
          /about1.md (About 1) //Tag: hide
          /about2.md (About 2) //Tag: hide
  projects/index.md (Projects)
          /projects1.md (Project 1)
          /projects2.md (Project 2)
          /projects3.md (Project 3)
  publication/index.md (Publication)
  archive/abc.md (ABC)
```

#### The navbar:

```yaml
About
Projects
Publication
```

#### The generated file list for Projects in `projects/index.md`:
(change the date_format in `configure.yml` - `date_format: %Y-%m-%d`)

```yaml
/yyyy-mm-dd/Project 1
/yyyy-mm-dd/Project 2
/yyyy-mm-dd/Project 3
```

The file list will not be generated on `about/index.md` because the special tag (Tag:hide) is implemented on both `about1.md` and `about2.md`.

#### meta data of the markdown (hide from generated list):

```markdown
---
Title: soonish
Description: soonish theme is a free and open source portfolio theme, featuring a responsive (queer) design and automatically generating directory listings for subfolders.
Author: soonish
Date: 2022-08-30
Robots: noindex,nofollow
Template: index
Tag: hide

---
```

## Installation

### Pico, version 1.x
Copy the content of the `soonish` directory in the `themes` folder of your **Pico** installation and change the following setting within your `config.php`:

```php
$config['theme'] = 'soonish';
```

### Pico, version 2.x
Copy the content of the `soonish` directory in the `themes` folder of your **Pico** installation and change the following setting within your `config.yml`:

```yaml
theme: soonish
```

## Other issues (want to fix)

- sort files: by order (meta.order) or by other means
- the list-style is not very stable, sometimes there is a space in between list item.
